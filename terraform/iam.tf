data "aws_iam_policy_document" "common_role_trust_policy_doc" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com", "s3.amazonaws.com", "states.amazonaws.com", "dynamodb.amazonaws.com"]
    }
    actions = [
      "sts:AssumeRole"
    ]
  }
}

resource "aws_iam_role" "common_role" {
  assume_role_policy = data.aws_iam_policy_document.common_role_trust_policy_doc.json
  name               = "common_iam_role_test_123"
}

data "aws_iam_policy_document" "common_policy_doc" {
  depends_on = [
    aws_s3_bucket.files_bucket,
    aws_dynamodb_table.files_table
  ]
  statement {
    sid    = "AllowLambdaStateFuncion"
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:CreateLogDelivery",
      "logs:GetLogDelivery",
      "logs:UpdateLogDelivery",
      "logs:DeleteLogDelivery",
      "logs:ListLogDeliveries",
      "logs:PutResourcePolicy",
      "logs:DescribeResourcePolicies",
      "lambda:*"
    ]
    resources = ["*"]
  }

  statement {
    sid    = "S3Bucket"
    effect = "Allow"
    actions = [
      "s3:ListAccessPointsForObjectLambda",
      "s3:GetAccessPoint",
      "s3:PutAccountPublicAccessBlock",
      "s3:ListAccessPoints",
      "s3:CreateStorageLensGroup",
      "s3:ListJobs",
      "s3:PutStorageLensConfiguration",
      "s3:ListMultiRegionAccessPoints",
      "s3:ListStorageLensGroups",
      "s3:ListStorageLensConfigurations",
      "s3:GetAccountPublicAccessBlock",
      "s3:ListAllMyBuckets",
      "s3:ListAccessGrantsInstances",
      "s3:PutAccessPointPublicAccessBlock",
      "s3:PutBucketNotification",
      "s3:CreateJob"
    ]
    resources = ["*"]
  }

  statement {
    sid    = "AllowS3Bucket"
    effect = "Allow"
    actions = [
      "s3:*"
    ]
    resources = [
      "${aws_s3_bucket.files_bucket.arn}"
    ]
  }

  statement {
    sid    = "AllowWriteOnDynamoDB"
    effect = "Allow"
    actions = [
      "dynamodb:*"
    ]
    resources = [
      "${aws_dynamodb_table.files_table.arn}"
    ]
  }

  statement {
    sid    = "AllowStepFunctions"
    effect = "Allow"
    actions = [
      "states:*"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "common_role_policy" {
  name   = "common_role_policy_test_123"
  policy = data.aws_iam_policy_document.common_policy_doc.json
}

resource "aws_iam_role_policy_attachment" "common_policy_attach" {
  role       = aws_iam_role.common_role.name
  policy_arn = aws_iam_policy.common_role_policy.arn
}


resource "random_string" "rand_string" {
  length  = 16
  special = false
  upper   = false
}

### Creating S3 Bucket with Encryption

resource "aws_s3_bucket" "files_bucket" {
  bucket        = "${var.bucket_name}-${random_string.rand_string.id}"
  force_destroy = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "bucket_encryption" {
  bucket = aws_s3_bucket.files_bucket.bucket
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = var.encryption_type
    }
  }
}

#### Creating DynamoDB Table

resource "aws_dynamodb_table" "files_table" {
  name           = var.db_table_name
  billing_mode   = "PROVISIONED"
  read_capacity  = 30
  write_capacity = "30"
  attribute {
    name = var.db_table_attribute
    type = "S"
  }
  hash_key = var.db_table_attribute
}

#### Creating Lambda Function

data "archive_file" "zip_lambda_code" {
  type        = "zip"
  source_dir  = "scripts/"
  output_path = "scripts/lambda.zip"
}

resource "aws_lambda_function" "s3_object_to_step_function" {
  depends_on    = [aws_sfn_state_machine.step_fuction_state_machine]
  filename      = "scripts/lambda.zip"
  function_name = var.lambda_function_name
  role          = aws_iam_role.common_role.arn
  handler       = "lambda.lambda_handler"
  runtime       = "python3.9"
  environment {
    variables = {
      SF_ARN = "${aws_sfn_state_machine.step_fuction_state_machine.arn}"
    }
  }
}

### Creating Step Function

resource "aws_sfn_state_machine" "step_fuction_state_machine" {
  name       = "StepFunction-${random_string.rand_string.id}"
  role_arn   = aws_iam_role.common_role.arn
  type       = "EXPRESS"
  definition = <<EOF
{
    "Comment":"Write S3 File Names to DynamoDB Table",
    "StartAt":"OnFileUpload",
    "States":{
        "OnFileUpload":{
            "Type":"Task",
            "Resource":"arn:aws:states:::dynamodb:putItem",
            "Parameters":{
                "TableName":"${var.db_table_name}",
                "Item":{
                    "${var.db_table_attribute}":{
                            "S.$":"$.key"
                    }
                }
            },
            "Retry":[
                {
                    "ErrorEquals":[
                        "States.ALL"
                    ],
                    "IntervalSeconds":1,
                    "MaxAttempts":3
                }
            ],
            "End":true,
            "ResultPath":"$.DynamoDB"
        }
    }

}
EOF
}

resource "aws_s3_bucket_notification" "bucket_upload_notification" {
  depends_on = [
    aws_lambda_permission.allow_bucket
  ]
  bucket = aws_s3_bucket.files_bucket.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_object_to_step_function.arn
    events              = ["s3:ObjectCreated:*"]
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.s3_object_to_step_function.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.files_bucket.arn
}
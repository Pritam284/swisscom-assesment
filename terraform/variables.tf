variable "bucket_name" {
  type    = string
  default = "file-upload-bucket"
}

variable "encryption_type" {
  type    = string
  default = "AES256"
}

variable "db_table_name" {
  type    = string
  default = "Files"
}

variable "db_table_attribute" {
  type    = string
  default = "FileName"
}

variable "lambda_function_name" {
  type    = string
  default = "Lambda_Trigger_Step_Function"
}

variable "aws_region" {
  type    = string
  default = "ap-southeast-1"
}

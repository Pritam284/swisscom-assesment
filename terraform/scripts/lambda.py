import os
import json
import boto3

STATE_FUNC_ARN = os.environ["SF_ARN"]

sf_client = boto3.client('stepfunctions')

def lambda_handler(event, context):
    print(event)
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = event['Records'][0]['s3']['object']['key']

    print(bucket)
    print(key)

    step_function_handler(key)

def step_function_handler(object_key):
    input_json = {
        'key': object_key
    }
    response = sf_client.start_execution(
            stateMachineArn=STATE_FUNC_ARN,
            input = json.dumps(input_json)
        )    

